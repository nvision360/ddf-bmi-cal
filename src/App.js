import React, { Component } from 'react';
import './App.css';
 
import Form from './components/dynamic-form/Form';

//JSON Data for form rendering
import BMI_JSON from './data/sample-bmi.json'; 
import HEAD_CIRCUMFERENCE_JSON from './data/sample-head-circumference.json';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      structure: null
    }
  }

  componentDidMount(){
    this.makeApiRequest();
  }

  makeApiRequest(){
    // Simuating API return data
    setTimeout(() => {
      this.setState({
        // structure: BMI_JSON,
        structure: HEAD_CIRCUMFERENCE_JSON,
      });      
    }, 1000);
  }

  handleFormResposne = (response) => {
    console.log("Response: ", response);
  }

  renderForm = () => {
    if(this.state.structure){
      return (
        <Form structure={this.state.structure} handleFormSubmit={this.handleFormResposne} />
      )
    }else{
      return (
        <div className="loading">Loading Form</div>
      )
    }
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          {this.renderForm()}
        </div>
      </div>
    );
  }
}

export default App;
