import React, { Component } from 'react'

export default class TextInput extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputValue: ""
    }
  }

  handleInputChange = (e) => {
    const value = e.target.value;
    const { element } = this.props;
    this.setState({
      inputValue: value
    })
    this.props.handleInputChange(element.id, value);
  }

  render() {
    const { element, index } = this.props;
    return (
      <div className="form-group input-box TextInput">
        <label htmlFor={element.id + index}>{element.displayName}: </label>
        <input
          type="text"
          className="form-control"
          id={element.id + index}
          placeholder={"Enter " + element.displayName}
          required={element.isRequired}
          pattern="^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$"
          title="Please enter a valid value"
          onChange={this.handleInputChange}
        />
      </div>
    )
  }
}
