import React, { Component } from 'react'

export default class NumberInput extends Component {

  constructor(props) {
    super(props);

    this.state = {
      inputvalue: ""
    }
  }

  handleInputChange = (e) => {
    const value = e.target.value;
    const { element } = this.props;
    this.setState({
      inputValue: value
    })
    this.props.handleInputChange(element.id, value);
  }

  renderAppender() {
    const { element } = this.props;
    if (element.unitOfMeasure) {
      return (
        <div className="input-group-append">
          <span className="input-group-text">{element.unitOfMeasure}</span>
        </div>
      )
    }
    return null;
  }

  render() {
    const { element, index } = this.props;
    const bound = {
      lowerLimit: element.bounds.lowerLimit ? element.bounds.lowerLimit : 0,
      upperLimit:  element.bounds.upperLimit
    }
    return (
      <div className="form-group input-box TextInput">
        <label htmlFor={element.id + index}>{element.displayName}: </label>
        <div className="input-group">
          <input
            type="number"
            className="form-control"
            id={element.id + index}
            placeholder={"Enter " + element.displayName}
            required={element.isRequired}
            min={bound.lowerLimit}
            max={bound.upperLimit}
            onChange={this.handleInputChange}
          />
          {this.renderAppender()}
        </div>
      </div>
    )
  }
}
