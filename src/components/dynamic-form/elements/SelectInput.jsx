import React, { Component } from 'react'

export default class SelectInput extends Component {

  constructor(props) {
    super(props);
    const defaultValue = this.setDefaultValue()
    this.state = {
      inputvalue: defaultValue
    }
  }

  setDefaultValue = () => {
    const { element } = this.props
    let defaultOption = element.options.find(option => option.isDefault);
    defaultOption = defaultOption ? defaultOption : element.options[0];

    // Send the default value to end result, in case user never changes the option
    this.props.handleInputChange(element.id, defaultOption.id)

    return defaultOption.id;
  }

  handleInputChange = (e) => {
    const value = e.target.value;
    const { element } = this.props;
    this.setState({
      inputValue: value
    })
    this.props.handleInputChange(element.id, value);
  }

  renderOptions = () => {
    const { element } = this.props
    let options = element.options.sort((a,b) => {
      return a.sortOrder - b.sortOrder
    })
    return options.map(option => {
      return (
        <option key={option.id} value={option.id}>{option.name}</option>
      )
    })
  }

  render() {
    const { element, index } = this.props;
    return (
      <div className="form-group input-box SelectInput">
        <label htmlFor={element.id + index}>{element.displayName}</label>
        <select 
          className="form-control" 
          id={element.id + index}
          value={this.state.inputValue}
          onChange={this.handleInputChange}
        >
          {this.renderOptions()}
        </select>
      </div>
    )
  }
}
