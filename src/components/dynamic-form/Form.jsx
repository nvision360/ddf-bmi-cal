import React, { Component } from 'react'

import './Form.css';

import TextInput from './elements/TextInput'
import NumberInput from './elements/NumberInput'
import SelectInput from './elements/SelectInput'

export default class Form extends Component {

  constructor(props) {
    super(props);
    const data = this.mineData();

    this.state = {
      structure: props.structure,
      inputs: data.inputs,
    }
  }

  /**
   * Function is responsible to mine input values to return back at the form submission
   */
  mineData = () => {
    let inputs = {};
    this.props.structure.dataElements.forEach(e => {
      inputs[e.id] = null
    });
    return { 
      inputs
    };
  }

  // Handler Functions
  /**
   * Handle the input changes
   */
  handleInputChange = (id, value) => {
    const { inputs } = this.state;
    inputs[id] = value
    this.setState({
      inputs
    })
  }

  /**
   * Handle Form submission
   */
  handleFormSubmit = (e) => {
    const { inputs } = this.state;
    e.preventDefault();
    if(inputs.bmi === null){
      inputs.bmi = Math.round((inputs.weight / (inputs.height/100)**2) * 100) / 100;
    }
    this.props.handleFormSubmit(this.state.inputs);
  }

  // Render Functions
  /**
   * Render Inputs from JSON (API)
   */
  renderInputs = (inputElements) => {
    return inputElements.map((inputElement, i) => {
      if (!inputElement.display) return null;
      switch (inputElement.type) {
        case 'textInput':
          return <TextInput 
            element={inputElement} 
            index={i} 
            key={i} 
            handleInputChange={this.handleInputChange} 
          />
        case 'numberInput':
          return <NumberInput 
            element={inputElement} 
            index={i} 
            key={i} 
            handleInputChange={this.handleInputChange} 
          />
        case 'select':
          return <SelectInput 
            element={inputElement} 
            index={i} 
            key={i} 
            handleInputChange={this.handleInputChange} 
          />
        default:
          return null
      }
    });
  }

  render() {
    const { structure } = this.state;
    return (
      <div className="Form">
        <div className="paper">
          <div className="title">
            {structure.observationName} - Form
          </div>

          <div className="body">
            <form onSubmit={this.handleFormSubmit}>
              {this.renderInputs(structure.dataElements)}

              <div className="submit-button">
                <button type="submit" className="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
